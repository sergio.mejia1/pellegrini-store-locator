import requests
import math
import time
import json

EARTH_RADIUS = 6.378e6


def build_request(lat: float, lon: float, space_distance_m: int = 500) -> dict:
    return {
        "TipoProdotto": "e",
        "Provenienza": "WEB",
        "Rete": 75,
        "SoloAIC": False,
        "Insegna": "",
        "Lat": lat,
        "Lng": lon,
        "UserNumMaxLocali": 20,
        "UserDistanzaMax": space_distance_m,
        "TipoLocale": [3, 6, 2, 4, 5, 1, 7, 8],
    }


def generate_grid(base_request, lat_n=5, lon_n=5):
    space = math.sqrt(2) * base_request["UserDistanzaMax"]
    lat_theta = space / EARTH_RADIUS
    point_list = []
    for i in range(lat_n):
        for j in range(lon_n):
            lon_theta = space / (
                EARTH_RADIUS * math.sin(math.radians(90 - base_request["Lat"]))
            )
            lat = base_request["Lat"] - math.degrees(lat_theta) * i
            lon = base_request["Lng"] + math.degrees(lon_theta) * j
            point_list.append({"Lat": lat, "Lng": lon})
    print(f"Created grid with {len(point_list)} points")
    return point_list


def query_pellegrini(request: dict, filters: dict[str, str]):
    URL = "https://appbpe.pellegrinicard.it/rest/api/Utilizzatore/TrovaLocali"
    Headers = {
        #'Accept': '*/*',
        #'Accept-Encoding': 'gzip, deflate, br',
        #'Accept-Language': 'en-US,en;q=0.9,it-IT;q=0.8,it;q=0.7,es;q=0.6',
        "Authorization": "Token cHViYmxpY28=",
        #'Content-Type': 'application/json',
        #'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Safari/537.36'
    }
    response = requests.post(url=URL, data=request, headers=Headers)
    dict_response = response.json()

    stores = {}
    for store in dict_response["Locali"]:
        keep = True
        for key, value in filters.items():
            if store[key] != value:
                keep = False

        if keep:
            reduced = {
                "id": store["ps_id"],
                "lat": store["ps_lat"],
                "lon": store["ps_lon"],
                "address": store["ps_IndCompleto"],
                "type": store["ps_tipolocale"],
                "name": store["ps_insegna"],
                "legit": None,
            }
            stores[reduced["id"]] = reduced
    print(
        "Found",
        len(stores),
        "stores near point (",
        request["Lat"],
        ",",
        request["Lng"],
        ")",
    )
    return stores


def query_pellegrini_grid(
    lat: float,
    lon: float,
    filters: dict[str, str],
    grid_lat_n: int,
    grid_lon_n: int,
    grid_space: float,
    sleep_time: float,
) -> dict:
    all_stores = {}
    request = build_request(lat, lon, grid_space)
    grid = generate_grid(request, grid_lat_n, grid_lon_n)
    for i, latlon in enumerate(grid):
        print(f"Request {i}/{len(grid)}:")
        request["Lat"] = latlon["Lat"]
        request["Lng"] = latlon["Lng"]
        stores = query_pellegrini(request, filters)
        all_stores.update(stores)
        time.sleep(sleep_time)
    return all_stores
