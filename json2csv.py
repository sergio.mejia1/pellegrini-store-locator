import csv


def write_csv(data: dict[str, dict], file_name):
    with open(file_name, "w+", newline="") as f:
        w = csv.DictWriter(f, list(data.values())[0].keys())
        w.writeheader()
        w.writerows(data.values())
