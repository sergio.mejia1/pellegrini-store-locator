import json2csv
import time
import json
import argparse, sys

from pellegrini import pellegrini

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "lat",
        help="Latitude of the north-west corner of the space to be queried",
        type=float,
    )
    parser.add_argument(
        "lon",
        help="Longitude of the north-west corner of the space to be queried",
        type=float,
    )
    parser.add_argument(
        "--filters",
        help="Path of JSON file containing filters to be applied. Filters must be simple key-value pairs",
        type=str,
    )
    parser.add_argument(
        "--grid-lat-n",
        default=10,
        help="""Height of the grid (along the latitude) to be queried
                      The total number of queries will be equal to grid_lat_n*grid_lon_n""",
        type=int,
    )
    parser.add_argument(
        "--grid-lon-n",
        default=10,
        help="""Height of the grid (along the latitude) to be queried
                      The total number of queries will be equal to grid_lat_n*grid_lon_n""",
        type=int,
    )
    parser.add_argument(
        "--space",
        default=500,
        help="""Space between query coordinates in meters. 
                      The total area queried by the program will be (grid_lat_n*space, grid_lon_n*space)
                      """,
        type=int,
    )
    parser.add_argument(
        "--sleep", default=0.5, help="Sleep time in seconds between queries", type=float
    )
    parser.add_argument("-o", "--output", help="Output csv file path")

    args = parser.parse_args()

    filters = {}
    if args.filters is not None:
        with open(args.filters, "r") as fp:
            filters = json.load(fp)

    all_stores = pellegrini.query_pellegrini_grid(
        lat=args.lat,
        lon=args.lon,
        filters=filters,
        grid_lat_n=args.grid_lat_n,
        grid_lon_n=args.grid_lat_n,
        grid_space=args.space,
        sleep_time=args.sleep,
    )
    print(all_stores)

    if args.output is not None:
        json2csv.write_csv(all_stores, args.output)
    else:
        print(all_stores)
