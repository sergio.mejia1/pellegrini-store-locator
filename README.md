# Pellegrini Map Maker

A python script to gather information of all shops on a defined zone advertised by Pellegrini.

## Motivation

Pellegrini website provides a search feature to look for places accepting these tickets. However, the UI is difficult to use, it only gives up to 20 places per request and is overall unreliable and buggy.

This script uses the same API that the official map uses, so the information is the same

## How to use

### Requirements: 

- Python >=3.10

The project provides both pyproject.toml to be used with poetry, or requirements.txt to be used by a generic environment.

The `main.py` is the entrypoint of the application. Arguments are provided by command line parameters. Execute `python main.py -h` (or `poetry run python main.py -h`) for more information.

**Example:**

To query all Pellegrini places in the area of Torino and output to a csv file, execute:

```bash
python main.py --grid-lat-n 22 --grid-lon-n 22 --space 500 --filters filters.json -o output.csv 45.1184392 7.5747143
```

The project provides a filters example for filtering places in the city of Torino located in `filters.json`. This filters are applied to the store object returned by the API, the most useful value being the `ps_indLocalita` which saves the town name of the store, so a filter can be applied to remove external towns if the area was set too big.

The positional `lat lon` parameters take the northwest corner of the grid to be queried. `--grid-lat-n 22 --grid-lat-n 22` parameters specified the size of the grid to be queried, the `--space` parameter specifies the query radius, so each point in the grid is separated by `sqrt(2)*space` meters to ensure full coverage. 

To calculate the grid size, consider the desired area to be queried and create the grid around it. I recommend leaving `--space` in its default value of 500m since the API endpoint returns maximum 20 places per query. Thus, to understand the number width and height of the grid, divide the width and height in kilometers of the city by sqrt(2)\*space. (i.e. Torino is roughly wrapped around a 15km*15km, so with `--space=500` we get the values of the grid size is around `15km/(1.41*0.5km) ~= 22` ). The resulting grid looks like:

![grid](img/grid.png)

This script will take around 22\*22\*0.5s = 242s ~= 4 minutes to execute. The reason for this is a 0.5s sleep timer between requests to avoid the API blocking the script. Modify this value by passing the amount in seconds between calls on the `--sleep` parameter.

### Using the output

You can use Google MyMaps to load this csv file and see all the points above a map. You can also share this map with edit permissions so others can see and modify it.

Schema

| Column name | type | description |
|:------------|:-----|:------------|
| id | integer | Unique id per store  |
| lat | number | Latitude |
| lon | number | Longitude |
| address | string | Physical address |
| type | string | Type of store (Bar, Ristorante, Tavola Calda, Alimentari, etc.) |
| name | string | Store name |
| legit | string | Empty field on purpose, to be filled if the store actually receives Pellegrini tickets | 